#pragma once

#include <VirtualBox.h>

using namespace System;
using namespace System::Text;

namespace Controllers{
	enum Environment_OS{
		OS_WIN7,
	};

	enum Environment_Arch{
		ARCH_X86,
		ARCH_X64
	};

	ref struct ControllerDescriptor{
	public:
		ControllerDescriptor(Environment_OS env_os, Environment_Arch env_arch,
			String ^machine_name, IMachine *machine){
			this->m_env_os = env_os;
			this->m_env_arch = env_arch;
			this->m_machine_name = machine_name;
			this->m_machine = machine;
		}
		Environment_OS get_env_os(){
			return m_env_os;
		}
		Environment_Arch get_env_arch(){
			return m_env_arch;
		}
		String ^get_machine_name(){
			return m_machine_name;
		}
		IMachine *get_machine(){
			return m_machine;
		}
	private:
		/* members */
		Environment_OS m_env_os;
		Environment_Arch m_env_arch;
		String ^m_machine_name;
		IMachine *m_machine;
	};

	typedef ControllerDescriptor CtrlDesc;
}