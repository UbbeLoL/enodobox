#pragma once
#include <VirtualBox.h>
#include "virtualization_controller_args.h"
#include "controller_descriptor.h"

namespace Controllers{
	ref class VirtualizationController
	{
	public:
		/* members */
		delegate void is_ready_del(VirtualizationController ^sender,
			VirtualizationControllerArgs ^args);
		event is_ready_del ^controller_ready;

		/* methods */
		VirtualizationController(ControllerDescriptor ^ctrl_desc);
		~VirtualizationController(void);
		HRESULT initialize_controller();
		HRESULT start_vm();

		/* properties */
		bool is_ready();

	private:
		/* methods */
		void set_ready();
		void set_machine_name();
		String ^generate_machine_name();
		void fetch_machine();
		bool verify_machine_availability();
		bool prepare_machine(ISession *session, IProgress *progress);
		void run_analysis(IConsole *console);

		/* members */
		IVirtualBox *m_virtual_box;
		bool m_is_ready;
		Session *m_vm_session;
		CtrlDesc ^m_ctrl_desc;
	};
}

