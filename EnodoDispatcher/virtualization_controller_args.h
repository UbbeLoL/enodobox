#pragma once
#include "controller_descriptor.h"

using namespace System;

namespace Controllers{
	ref class VirtualizationControllerArgs 
		: public EventArgs{
	public:
		/* members */
		ControllerDescriptor ^ctrl_desc;

		/* methods */
		VirtualizationControllerArgs(CtrlDesc ^ctrl_desc);
		~VirtualizationControllerArgs(void);
	};
}
