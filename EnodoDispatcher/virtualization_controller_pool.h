#pragma once
#include "virtualization_controller.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace Controllers;

ref class VirtualizationControllerPool
{
public:
	VirtualizationControllerPool(Environment_OS env_os, Environment_Arch env_arch,
		IVirtualBox *virtual_box);
	void load_descriptors();

private:
	/* methods */
	bool probe_next_vm(CtrlDesc ^%ctrl_desc);
	bool probe_next_available_vm(Int32 ^%idx);
	String^ generate_machine_name();
	void launch_vm();

	/* members */
	List<VirtualizationController^> ^m_ctrl_pool;
	Environment_OS m_env_os;
	Environment_Arch m_env_arch;
	int m_vm_count;
	IVirtualBox *m_virtual_box;
};


typedef VirtualizationControllerPool VcPool;
