#include "virtualization_controller.h"
#include <stdio.h>
#include "marshalling_helper.h"

using namespace Controllers;
using namespace System;
using namespace System::Text;
using namespace System::Runtime::InteropServices;

static const OLECHAR *LOADER_PATH = L"C:\\Users\\Enodo\\Desktop\\ConsoleApplication1.exe";
static const OLECHAR *VM_USER = L"Enodo";
static const OLECHAR *SNAP_NAME = L"clean_state";

#pragma region public methods

VirtualizationController::VirtualizationController(ControllerDescriptor ^ctrl_desc){
	this->m_ctrl_desc = ctrl_desc;
	this->m_is_ready = true;
	this->m_virtual_box = NULL;
//	this->m_machine = NULL;

	this->set_machine_name();
}

VirtualizationController::~VirtualizationController(void){
	CoUninitialize();
}

HRESULT VirtualizationController::initialize_controller(){
	HRESULT res;
	IVirtualBox *virtualBox;

	// Create the root VirtualBox object
	res = CoCreateInstance(CLSID_VirtualBox,
                              NULL,
                              CLSCTX_LOCAL_SERVER,
                              IID_IVirtualBox,
							  (void**)&virtualBox);
	m_virtual_box = virtualBox;
	if (!SUCCEEDED(res)){
		throw gcnew Exception();
	}

	this->fetch_machine();
	this->set_ready();
	return res;
}

bool VirtualizationController::is_ready(){
	return this->m_is_ready;
}

HRESULT VirtualizationController::start_vm(){
	HRESULT rc = NULL;
	if(!this->is_ready() || !this->verify_machine_availability()){
		throw gcnew Exception("Could not verify machine availability");
	}

    ISession *session = NULL;
    IConsole *console = NULL;
    IProgress *progress = NULL;
    BSTR sessiontype = SysAllocString(L"gui");
    BSTR guid;

    rc = this->m_ctrl_desc->get_machine()->get_Id(&guid);
    if (!SUCCEEDED(rc)){
        throw gcnew Exception("Error retrieving machine ID");
    }

    rc = CoCreateInstance(CLSID_Session,
                            NULL,
                            CLSCTX_INPROC_SERVER,
                            IID_ISession,
                            (void**)&session);
    if (!SUCCEEDED(rc)){
        throw gcnew Exception("Error creating Session instance");
    }

	this->m_ctrl_desc->get_machine()->LockMachine(session, LockType_Shared);
	if(!this->prepare_machine(session, progress)){
		throw gcnew Exception("Failed to prepare machine");
	}
	session->UnlockMachine();

    rc = this->m_ctrl_desc->get_machine()->LaunchVMProcess(session, sessiontype,
                                    NULL, &progress);
    if (!SUCCEEDED(rc)){
        throw gcnew Exception("Could not open remote session");
    }

    rc = progress->WaitForCompletion(-1);
    session->get_Console(&console);

	this->run_analysis(console);

    printf("Press enter to power off VM and close the session...\n");
    getchar();

    rc = console->PowerDown(&progress);

    printf("Powering off VM, please wait ...\n");
    rc = progress->WaitForCompletion(-1);
    rc = session->UnlockMachine();

	return rc;
}

#pragma endregion

#pragma region private methods

void VirtualizationController::set_ready(){
	this->m_is_ready = true;
	controller_ready(this, gcnew VirtualizationControllerArgs(this->m_ctrl_desc));
}

void VirtualizationController::set_machine_name(){
	
}

String^ VirtualizationController::generate_machine_name(){
	StringBuilder strOut;

	strOut.Append("AnalysisBox");
	switch (this->m_ctrl_desc->get_env_os()){
	case Controllers::OS_WIN7:
		strOut.Append("_W7");
		break;
	default:
		//TODO: err?
		break;
	}
	switch (this->m_ctrl_desc->get_env_arch()){
	case Controllers::ARCH_X86:
		strOut.Append("_32");
		break;
	case Controllers::ARCH_X64:
		strOut.Append("_64");
		break;
	default:
		//TODO: err?
		break;
	}
	return strOut.ToString();
}

void VirtualizationController::fetch_machine(){
	BSTR vmName = MarshallingHelper::mngd_string_to_bstr(this->m_ctrl_desc->get_machine_name());
	IMachine *machine = NULL;
	HRESULT res = this->m_virtual_box->FindMachine(vmName, &machine);
	SysFreeString(vmName);
	if(!SUCCEEDED(res)){
		throw gcnew Exception("Failed to find machine " + this->m_ctrl_desc->get_machine_name());
	}
	//this->m_machine = machine;
}

bool VirtualizationController::verify_machine_availability(){
	MachineState state;
	HRESULT res = this->m_ctrl_desc->get_machine()->get_State(&state);
	if(!SUCCEEDED(res)){
		return false;
	}

	BSTR snapName = SysAllocString(SNAP_NAME);
	ISnapshot *snap;
	res = this->m_ctrl_desc->get_machine()->FindSnapshot(snapName, &snap);
	SysFreeString(snapName);
	snap->Release();
	snap = NULL;

	return SUCCEEDED(res) && state == MachineState_PoweredOff || state == MachineState_Aborted;
}

bool VirtualizationController::prepare_machine(ISession *session, IProgress *progress){
	HRESULT rc;
	BSTR snapName = SysAllocString(SNAP_NAME);
	ISnapshot *snap;
	// We assume it works after verify_machine_availability
	rc = this->m_ctrl_desc->get_machine()->FindSnapshot(snapName, &snap);
	SysFreeString(snapName);

	IMachine *mutablemachine;
	rc = session->get_Machine(&mutablemachine);
	if(!SUCCEEDED(rc)){
		return false;
	}

	rc = mutablemachine->RestoreSnapshot(snap, &progress);
		if(!SUCCEEDED(rc)){
		return false;
	}
	snap->Release();
	snap = NULL;

	rc = progress->WaitForCompletion(-1);
		if(!SUCCEEDED(rc)){
		return false;
	}

	rc = mutablemachine->SaveSettings();
	mutablemachine->Release();
	mutablemachine = NULL;
	return SUCCEEDED(rc);
}

void VirtualizationController::run_analysis(IConsole *console){
	IGuest *guest;
	IGuestSession *guestSession;
	HRESULT rc;
	rc = console->get_Guest(&guest);
	BSTR name = SysAllocString(VM_USER);
	rc = guest->CreateSession(name, NULL, NULL, NULL, &guestSession);
	BSTR proc = SysAllocString(LOADER_PATH);
}

#pragma endregion