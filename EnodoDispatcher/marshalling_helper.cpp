#include "marshalling_helper.h"

using namespace System::Runtime::InteropServices;

BSTR MarshallingHelper::mngd_string_to_bstr(String ^str){
	// For some reason Marshal::StringToBSTR doesn't work
	WCHAR *buff = (WCHAR*)Marshal::StringToHGlobalUni(str).ToPointer();
	// Make sure to free after using...
	return SysAllocString(buff);
}
