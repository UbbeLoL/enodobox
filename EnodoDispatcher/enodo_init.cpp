#include <Windows.h>
#include "virtualization_controller_pool.h"


using namespace System;
using namespace Controllers;

[STAThread]
void Main(array<String^>^ args)
{
	/* initialize COM subsystem on main thread */
	CoInitialize(NULL);

	/* create root virtual box object */
	HRESULT res;
	IVirtualBox *virtualBox;

	/* Create the root VirtualBox object */
	res = CoCreateInstance(CLSID_VirtualBox,
                              NULL,
                              CLSCTX_LOCAL_SERVER,
                              IID_IVirtualBox,
							  (void**)&virtualBox);

	if (!SUCCEEDED(res)){
		throw gcnew Exception("Failed to create root virtualbox object");
	}

	/* create controller pools */
	VcPool ^win7_64_pool = gcnew VcPool(OS_WIN7, ARCH_X64, virtualBox);

	/* load descriptors/VMs for controller pools */
	win7_64_pool->load_descriptors();

	Console::ReadLine();
}

