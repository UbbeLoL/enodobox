#include "virtualization_controller_pool.h"
#include "marshalling_helper.h"

using namespace System::Threading;

#pragma region public methods

VirtualizationControllerPool::VirtualizationControllerPool(Environment_OS env_os,
														   Environment_Arch env_arch,
														   IVirtualBox *virtual_box){
	this->m_env_os = env_os;
	this->m_env_arch = env_arch;
	this->m_virtual_box = virtual_box;
	this->m_vm_count = 0;
	this->m_ctrl_pool = gcnew List<VirtualizationController^>;
}

void VirtualizationControllerPool::load_descriptors(){
	CtrlDesc ^cur_desc;
	while(this->probe_next_vm(cur_desc)){
		VirtualizationController ^ctrl = gcnew VirtualizationController(cur_desc);
		m_ctrl_pool->Add(ctrl);
	}

	//Thread^ thisThread = gcnew Thread(
	//	gcnew ThreadStart(this,&VirtualizationControllerPool::launch_vm));

	//Thread^ thatThread = gcnew Thread(
	//	gcnew ThreadStart(this,&VirtualizationControllerPool::launch_vm));

	//this->m_vm_count = 0;

	//thisThread->Start();
	//thatThread->Start();
}

void VirtualizationControllerPool::launch_vm(){
	CoInitialize(NULL);
	this->m_ctrl_pool[this->m_vm_count++]->start_vm();
}

#pragma endregion

#pragma region private methods

bool VirtualizationControllerPool::probe_next_vm(CtrlDesc ^%ctrl_desc){
	String ^mngd_name = generate_machine_name();
	BSTR vmName = MarshallingHelper::mngd_string_to_bstr(mngd_name);
	IMachine *machine = NULL;
	HRESULT res = this->m_virtual_box->FindMachine(vmName, &machine);
	SysFreeString(vmName);

	if(!SUCCEEDED(res)){
		return false;
	}

	ctrl_desc = gcnew CtrlDesc(
		this->m_env_os,
		this->m_env_arch,
		mngd_name,
		machine);
	return true;
}

String^ VirtualizationControllerPool::generate_machine_name(){
	StringBuilder strOut;

	strOut.Append("AnalysisBox");
	switch (m_env_os){
	case Controllers::OS_WIN7:
		strOut.Append("_W7");
		break;
	default:
		//TODO: err?
		break;
	}
	switch (m_env_arch){
	case Controllers::ARCH_X86:
		strOut.Append("_32_");
		break;
	case Controllers::ARCH_X64:
		strOut.Append("_64_");
		break;
	default:
		//TODO: err?
		break;
	}
	strOut.Append((this->m_vm_count++).ToString());
	return strOut.ToString();
}

#pragma endregion